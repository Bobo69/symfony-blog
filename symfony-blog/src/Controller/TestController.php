<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class TestController extends AbstractController {

/**
 * @Route("/", name="test")
 */

  public function index() {

      $maVariable = "bloup";

    return $this->render("test.html.twig", ["write" => $maVariable
    ]);
  }
}