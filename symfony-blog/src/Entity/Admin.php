<?php

namespace App\Entity;

class Admin {
  public $id;
  public $pseudo;
  public $mail;
  public $password;

  public function __construct(int $paramId = null, string $paramPseudo = "", string $paramMail = "", string $paramPassword = null) {
    $this->pseudo = $paramPseudo;
    $this->mail = $paramMail;
    $this->password =$paramPassword;
  }
}
