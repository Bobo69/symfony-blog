-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'Apple Watch','De quoi susciter une grande curiositÃ© auprÃ¨s des professionnels de santÃ©, et sur les rÃ©seaux sociaux, emballant ainsi le petit monde du numÃ©rique. AprÃ¨s son capteur de glycÃ©mie actuellement en projet (sans percer la peau) dâ€™aprÃ¨s les informations circulant sur le net, la marque Ã  la pomme semble donc dÃ©cidÃ©e Ã  miser sur la santÃ© pour attirer de nouveaux consommateurs et sÃ©duire, pourquoi pas, les professionnels de santÃ©. Quels meilleurs ambassadeurs que des praticiens recommandant massivement les produits connectÃ©s dâ€™Apple ?','Tech','0000-00-00',''),(3,'Caesio teres','Caesio teres est un poisson de petite taille pouvant atteindre 40 cm de long1. Son corps est fusiforme, il a une petite bouche protractile (c\'est-Ã -dire qu\'elle peut Ãªtre projetÃ©e vers l\'avant pour mieux capter la nourriture) et une nageoire caudale fourchue. Sa livrÃ©e est bleue et jaune sur le dos. Lorsque l\'individu est jeune, la zone jaune s\'Ã©tend de la nuque Ã  la nageoire caudale traÃ§ant une diagonale qui dÃ©bute Ã  la base de la partie antÃ©rieure de la nageoire dorsale et se poursuit jusque la base infÃ©rieure du pÃ©doncule caudal. Cette zone jaune se rÃ©duit avec l\'Ã¢ge Ã  la nageoire caudale et au pÃ©doncule caudal.','Animaux','0000-00-00',''),(4,'Vincent-Nicolas Raverat','Â« Raverat (Vincent-Nicolas), peintre d\'histoire, nÃ© Ã  Moutier-Saint-Jean (CÃ´te-d\'Or) le 22 janvier 1801, Ã©tait Ã©lÃ¨ve de Delacluze (sic), d\'Abel de Pujol, et de l\'Ã‰cole des beaux-arts, oÃ¹ il entra le 4 mars 1815. Il dÃ©buta au Salon de 1831, avec deux tableaux : l\'EspÃ©rance, et HÃ©ro tremblant pour les jours de LÃ©andre ; il exposa ensuite en 1833, 1834, 1835, 1836, 1837, 1838. 1839, 1840, 1842, 1843, 1845, 1847, 1848, 1855, et cette annÃ©e mÃªme on voyait de lui au palais des Champs-Ã‰lysÃ©es le portrait au crayon d\'Abel de Pujol. Raverat, qui est mort Ã  Paris le 30 juin dernier, avait obtenu en 1837 une mÃ©daille de 3e classe pour son Christ expirant, que possÃ¨de l\'Ã©glise de Saulieu ; au nombre de ses ouvrages, nous rappellerons : les Enfants de Jacques d\'Armagnac, duc de Nemours, sous lâ€™Ã©chafaud de leur pÃ¨re ; le duc d\'OrlÃ©ans rÃ©gent et la duchesse du Maine (ces deux toiles sont en Russie) ; JÃ©sus prÃ¨s d\'Ãªtre crucifiÃ© (tableau achetÃ© par l\'Ã‰tat) ; les Martyrs de CrÃ©teil (dans l\'Ã©glise de cette ville) ; plusieurs de ses ouvrages figurent dans les musÃ©es de Dijon et de Semur ; la Prise de Fumes le 11 juillet 1744, et une douzaine de portraits dans les galeries de Versailles ; deux tableaux au MinistÃ¨re du commerce ; neuf figures sur fond d\'or dans l\'hÃ©micycle de la Madeleine ; enfin une peinture murale allÃ©gorique d\'une vÃ©ritable importance, au haut de l\'escalier de l\'Ã‰cole des mines. Raverat Ã©tait fixÃ© Ã  Chartres depuis quelques mois, oÃ¹ il prÃªtait, pour l\'exÃ©cution des cartons, son concours Ã  M. Lorin, qui vient de fonder Ã  Saint-Cheron, l\'un des faubourgs de la capitale de la Beauce, un Ã©tablissement de vitraux peints. Â»\r\n\r\nâ€” E. B. De L. Â« NÃ©crologie Â», La chronique des arts et de la curiositÃ©. SupplÃ©ment Ã  la Gazette des Beaux-arts, tome III, 1865, p. 254','Histoire','0000-00-00','');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-30 22:54:40
